#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    #get list of numbers
    numbers = []
    with open('aoc2020_puzzle1_input.txt') as f:
        for number in f:
            number = int(number)
            numbers.append(number)

#.pop(index) Remove an item and get it's value.
    for i in range(0,len(numbers)):
        # get a copy of the list
        numbers_copy = numbers.copy()
        first_number = numbers_copy.pop(i)
        for second_number in numbers_copy:
            if first_number + second_number == 2020:
                print(first_number * second_number, "Winner")




if __name__ == '__main__':
    main()