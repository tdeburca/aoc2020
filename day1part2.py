#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
import sys

def main():
    #get list of numbers
    numbers = []
    with open('aoc2020_puzzle1_input.txt') as f:
        for number in f:
            number = int(number)
            numbers.append(number)

    results = []
    for i in range(0,len(numbers)):
        # get a copy of the list
        numbers_copy = numbers.copy()
        first_number = numbers_copy.pop(i)
        for second_number in numbers_copy:
            total = first_number + second_number
            results.append([first_number,second_number,total])

    for third_number_index in range(0,len(numbers)):
        third_number = numbers[third_number_index]
        for x in results:
            if(x[2] + third_number == 2020):
                print(x, third_number, x[0] * x[1] * third_number)



if __name__ == '__main__':
    main()