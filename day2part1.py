#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    data = getData()
    # 1-3 a: abcde
    # 1-3 b: cdefg
    #2-9 c: ccccccccc
    count = 0
    for record in data:
        low = record['low']
        high = record['high']
        letter = record['letter']
        password = record['password']
        if low <= password[letter] <= high:
            count += 1
    print(count)

def countLetters(password):
    out = {'a': 0, 'b': 0, 'c': 0, 'd': 0, 'e': 0, 'f': 0, 'g': 0,
           'h': 0, 'i': 0, 'j': 0, 'k': 0, 'l': 0, 'm': 0, 'n': 0,
           'o': 0, 'p': 0, 'q': 0, 'r': 0, 's': 0, 't': 0, 'u': 0, 
           'v': 0, 'w': 0, 'x': 0, 'y': 0, 'z': 0}
    for letter in password:
        out[letter] +=  1
    return out

def getData(): 
    data = []
    with open('day2_input.txt') as f:
        for entry in f:
            # split on :
            rule, password = entry.split(':') 
            # remove carriage return from password.
            password = password.strip()
            # split on space
            instances, letter = rule.split(' ')
            instances = instances.split('-')
            password = countLetters(password)
            data.append({'low':int(instances[0]), 'high':int(instances[1]), 'letter':letter, 'password':password})
    return data

  



if __name__ == '__main__':
    main()