#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    data = getData()
    count = 0
    for record in data:
        count = count + testValidPassword(record)
    print(count)

def testValidPassword(record):
        low = record['low']
        high = record['high']
        letter = record['letter']
        password = record['password']
        instance = 0
        if letter == password[low - 1]:
            instance += 1
        if letter == password[high - 1]:
            instance += 1
        if instance == 1:
            return 1
        else:
            return 0

def getData(): 
    data = []
    with open('day2_input.txt') as f:
        for entry in f:
            # split on :
            rule, password = entry.split(':') 
            # remove carriage return from password.
            password = password.strip()
            # split on space
            instances, letter = rule.split(' ')
            instances = instances.split('-')
            data.append({'low':int(instances[0]), 'high':int(instances[1]), 'letter':letter, 'password':password})
    return data

if __name__ == '__main__':
    main()