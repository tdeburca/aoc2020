#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    data = getData()
    # prettyOut(data)
    x,y = 1,0
    count = 0
    # How often does the pattern repeat?
    repeat_freq = len(data[0])
    number_of_rows= len(data)
    # right 3, down 1:
    for step in range(1,len(data)):
        x += 3
        y += 1
        if x > repeat_freq:
            x -= repeat_freq
        spacer = '-'
        # print(data[y])
        # print(split(spacer.replace('-', '-'*x,1)))
        count += isTree(data,x,y)
    print(count)
        
        
        

def isTree(data, x,y):
    if data[y][x-1] == '#':
        return 1
    else:
        return 0

def prettyOut(data):
    for i in data:
        print(len(i))

def getData(): 
    data = []
    with open('day3_input.txt') as f:
    #with open('day3testdata.txt') as f:
        for entry in f:
            data.append(split(entry.strip()))
    return data

def split(word): 
    return [char for char in word]

  



if __name__ == '__main__':
    main()