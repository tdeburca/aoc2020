#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    data = getData()
    total = 0
    # prettyOut(data)
    # Right 1, down 1.
    step_down = 1
    step_right = 1
    total = getTrees(data, step_down,step_right) # 

    # Right 3, down 1. (This is the slope you already checked.)
    step_down = 1
    step_right = 3
    total = total * getTrees(data, step_down,step_right)# 

    #Right 5, down 1.
    step_down = 1
    step_right = 5
    total = total * getTrees(data, step_down,step_right)

    #Right 7, down 1.
    step_down = 1
    step_right = 7
    total = total * getTrees(data, step_down,step_right)

    #Right 1, down 2.
    step_down = 2
    step_right = 1
    total = total * getTrees(data, step_down,step_right)

    print(total)

def getTrees(data,step_down, step_right):
    count = 0
    x,y = 1,0
    repeat_freq = len(data[0])
    for step in range(1,len(data)):
        x += step_right
        y += step_down
        # Prevent overrunning end of file.
        if y > len(data):
            return count
        # reset count to repeat pattern
        if x > repeat_freq:
            x -= repeat_freq
        # spacer = '-'
        # print(data[y])
        # print(split(spacer.replace('-', '-'*x,1)))
        count += isTree(data,x,y)
    return count
        
def isTree(data, x,y):
    if data[y][x-1] == '#':
        return 1
    else:
        return 0

def prettyOut(data):
    for i in data:
        print(len(i))

def getData(): 
    data = []
    with open('day3_input.txt') as f:
    #with open('day3testdata.txt') as f:
        for entry in f:
            data.append(split(entry.strip()))
    return data

def split(word): 
    return [char for char in word]

if __name__ == '__main__':
    main()