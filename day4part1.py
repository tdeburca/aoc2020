#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    data = getData()
    count = countValidPassports(data)
    print(count)

def countValidPassports(data):
    # required_fields = {'byr','iyr','eyr','hgt','hcl','ecl','pid','cid'}
    required_fields = {'byr','iyr','eyr','hgt','hcl','ecl','pid'}
    total = 0
    for record in data:
        fields = set(record.keys())
        if required_fields.issubset(fields):
            total += 1
    return total

def prettyOut(data):
    for i in data:
        print(i)

def getData(): 
    data = []
    with open('day4data.txt') as f:
    # with open('day4testdata.txt') as f:
        d = {}
        for entry in f:
            # make the lines into lists
            entry = entry.split()
            # use empty lists as record breaks.
            if len(entry) != 0:
                for field in entry:
                    key, value = field.split(':')
                    d[key] = value
            else:
                data.append(d)
                d = {}
    return data

def split(word): 
    return [char for char in word]

if __name__ == '__main__':
    main()