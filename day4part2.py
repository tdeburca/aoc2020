#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    data = getData()
    data = checkValidFields(data)
    data = checkFieldValues(data)
    print(len(data))

def checkFieldValues(data):
    # byr (Birth Year) - four digits; at least 1920 and at most 2002.
    # iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    # eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    # hgt (Height) - a number followed by either cm or in:
    # If cm, the number must be at least 150 and at most 193.
    # If in, the number must be at least 59 and at most 76.
    # hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    # ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    # pid (Passport ID) - a nine-digit number, including leading zeroes.
    # cid (Country ID) - ignored, missing or not.
    out = []
    for record in data:
        # check valid years
        if (1290 <= int(record['byr']) <=2002) and (2010 <= int(record['iyr']) <= 2020) and (2020 <= int(record['eyr']) <= 2030):
            if heightOK(record['hgt']):
                if hclOK(record['hcl']):
                    if record['ecl'] in ['amb','blu','brn','gry','grn','hzl','oth']:
                        if len(record['pid']) == 9:
                            if record['pid'].isdecimal():
                                out.append(record)
    return out

def hclOK(h):
    if len(h) == 7:
        if h[0] == '#':
            number = h[1:]
            try:
                int(number, 16)
                return True
            except:
                return False
    else: 
        return False

def heightOK(h):
    unit = h[-2:]
    # case with no units.
    if not unit.isalpha():
        return False
    height = int(h[:-2])
    if unit == 'cm':
        if 150 <= height <= 193:
            return True 
    if unit == 'in':
        if 59 <= height <= 76:
            return True
    else:
        return False
    
def checkValidFields(data):
    # required_fields = {'byr','iyr','eyr','hgt','hcl','ecl','pid','cid'}
    required_fields = {'byr','iyr','eyr','hgt','hcl','ecl','pid'}
    out = []
    for record in data:
        fields = set(record.keys())
        if required_fields.issubset(fields):
            out.append(record)
    return out

def prettyOut(data):
    for i in data:
        print(i)

def getData(): 
    data = []
    with open('day4data.txt') as f:
    # with open('day4testdata.txt') as f:
        d = {}
        for entry in f:
            # make the lines into lists
            entry = entry.split()
            # use empty lists as record breaks.
            if len(entry) != 0:
                for field in entry:
                    key, value = field.split(':')
                    d[key] = value
            else:
                data.append(d)
                d = {}
    return data

if __name__ == '__main__':
    main()