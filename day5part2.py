#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    data = getData()

    # FBFBBFFRLR
    # 128 Rows
    # B = Upper Half
    # F = Lower Half
    # Left Right
    # L = Lower Half 
    # F = Upper Half
    # test = ['BFFFBBFRRR', 'FFFBBBFRRR', 'BBFFBBFRLL']
    # boarding_pass = 'FBFBBFFRLR'
    passes = set()
    for i in data:
        passes.add(getboarding_Pass(i)) 
    first_seat = min(passes)
    last_seat = max(passes)
    for seat in range(first_seat,last_seat+1):
        if seat not in passes:
            print(seat)
            break



def getboarding_Pass(boarding_pass):
    row = int(boarding_pass[:7].replace("F","0").replace("B","1"),2)
    column = int(boarding_pass[7:].replace("R","1").replace("L","0"),2)
    return ((row * 8) + column)



def prettyOut(data):
    for i in data:
        print(i)

def getData(): 
    data = []
    with open('day5data.txt') as f:
    # with open('day4testdata.txt') as f:
        for entry in f:
            # make the lines into lists
            data.append(entry)
    return data

if __name__ == '__main__':
    main()