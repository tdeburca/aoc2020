#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    data = getData()
    #count entries.
    count = 0
    for i in data:
        count += len(i)
    print(count)


def prettyOut(data):
    for i in data:
        print(i)

#returns list of sets
def getData(): 
    data = []
    with open('day6data.txt') as f:
    # with open('day6testdata.txt') as f:
        s = set()        
        for entry in f:
            l = []
            l[:] = entry.strip()
            entry = l
            if len(entry) != 0:
                for field in entry:
                    s.add(field)
            else:
                data.append(s)
                s = set()
    return data

if __name__ == '__main__':
    main()