#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    data = getData()
    # all questions answerd in group. 
    count = 0
    for group in data:
        count += getGroupTotals(group)
    print(count)

def getGroupTotals(group):
        answered_questions = set()
        for person in group:
            for answer in person:
                answered_questions.add(answer)
        count = 0
        # check if response is in EVERY persons answer.
        d = {}
        for i in answered_questions:
            d[i]=0
        for person in group:
            for answer in person:
                d[answer] += 1
        # For key, if value == people in group, add to 'all_answered'
        for k in d.keys():
            if d[k] == len(group):
                    count += 1
        return count

def prettyOut(data):
    for i in data:
        print(i)

#returns list of sets
def getData(): 
    data = []
    with open('day6data.txt') as f:
    # with open('day6testdata.txt') as f:
        answer = []
        for passenger in f:
            passenger = passenger.strip()
            if len(passenger) > 0:
                answer.append(passenger)
            else:
                data.append(answer)
                answer = []   
    return data

if __name__ == '__main__':
    main()