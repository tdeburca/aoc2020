#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

def main():
    bagmap= {'lr': {'bw':1, 'my':2},
            'do': {'bw':3, 'my':4},
            'bw': {'sg':1},
            'my': {'sg':2, 'fb':9},
            'sg': {'do':1, 'vp':2},
            'do': {'fb':3, 'db':4},
            'vp': {'fb':5, 'db':6},
            'fb': {},
            'db': {}}
    # how many colors can, eventually, contain at least one shiny gold bag?
    # sg?
    bag_types = list(bagmap.keys())

    #explore one bag:
    start_node = 'lr'
    to_visit = [start_node] # list with one item
    visited = [] # empty I have not visited anything

    while to_visit:
        node_to_checkout = to_visit.pop(0)
        for neighbor_node in bagmap[node_to_checkout]:
            if neighbor_node not in visited:
                to_visit.insert(0, neighbor_node) # put in route plan
                print('to_visit', to_visit)
                visited.append(neighbor_node)  # note that we already added
                print('visited', visited)

    print('final:', visited)

    # data = getData()

def prettyOut(data):
    for i in data:
        print(i)

def getData(): 
    data = []
    with open('day6data.txt') as f:
    # with open('day6testdata.txt') as f:
        for line in f:
            data.append(f)
    return data

if __name__ == '__main__':
    main()